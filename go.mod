module gitlab.com/omnifi/automaatio/pulumi

go 1.22.1

require (
	github.com/spf13/cobra v1.8.0
	gitlab.com/omnifi/automaatio/io v0.0.0
	gitlab.com/omnifi/automaatio/net v0.0.0
)

require golang.org/x/mod v0.16.0

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)

replace gitlab.com/omnifi/automaatio/io => gitlab.com/omnifi/automaatio/io.git v0.0.0

replace gitlab.com/omnifi/automaatio/net => gitlab.com/omnifi/automaatio/net.git v0.0.0
