package install

import (
	"testing"

	"gitlab.com/omnifi/automaatio/pulumi/environment"
)

// TestIsNotInstalled tests the `IsInstalled` method when Pulumi is not installed.
func TestIsNotInstalled(t *testing.T) {
	installed := environment.IsInstalled()

	if installed {
		t.Fail()
	}
}

// TestIsInstalled tests the `IsInstalled` method when Pulumi is installed.
// To ensure this works correctly Pulumi is installed with the `Install` method.
func TestIsInstalled(t *testing.T) {
	_, err := Install()

	if err != nil {
		t.Fail()
	}

	installed := environment.IsInstalled()

	if !installed {
		t.Fail()
	}
}
