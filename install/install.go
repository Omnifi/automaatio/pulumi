package install

import (
	"fmt"
	"os"

	"gitlab.com/omnifi/automaatio/pulumi/download"
	"gitlab.com/omnifi/automaatio/pulumi/environment"
)

// Install ensures Pulumi is available on the PATH environment variable. If
// Pulumi is not invocable (and version 3) then this method will download the
// toolkit for this target platform and architecture, and add it to the PATH
// for this session only.
//
// The path of the binaries is returned if installed. Otherwise an error is
// returned.
func Install() (string, error) {
	if environment.IsInstalled() {
		return environment.GetExecutablePath(), nil
	}

	if !download.IsDownloaded() {
		download.Download()
	}

	binPath := environment.GetExecutablePath()

	os.Setenv("PATH", fmt.Sprintf("%s:%s", os.Getenv("PATH"), binPath))

	if environment.IsInstalled() {
		return binPath, nil
	}

	return "", &InstallError{
		message: "pulumi could not be installed",
	}
}
