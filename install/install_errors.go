package install

import (
	"fmt"
)

// InstallError is used to signal a failed operation due to
// an issue with the Pulumi installation.
type InstallError struct {
	// Error to pass.
	message string
}

// Returns the error message.
func (pie *InstallError) Error() string {
	return fmt.Sprintf("pulumi install error: %v", pie.message)
}
