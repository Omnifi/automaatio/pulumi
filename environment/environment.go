package environment

import (
	"os"
	"os/exec"
	"path/filepath"

	"golang.org/x/mod/semver"
)

// GetExecutablePath returns the path where the Pulumi binaries are
// installed (by this package).
func GetExecutablePath() string {
	workingPath := GetWorkingPath()

	return filepath.Join(workingPath, "pulumi")
}

// GetWorkingPath returns the path where Pulumi is installed from the
// extraction.
func GetWorkingPath() string {
	workingDirectory, err := os.Getwd()

	if err != nil {
		return ""
	}

	pulumiPath := filepath.Join(workingDirectory, ".pulumi")

	return pulumiPath
}

// IsInstalled confirms whether Pulumi is installed and is the
// correct version (v3).
func IsInstalled() bool {
	cmd := exec.Command("pulumi", "version")

	output, err := cmd.Output() // Run()

	if err != nil {
		return false
	}

	semverComparison := semver.Compare("v3", string(output))

	if semverComparison == 1 {
		return true
	} else {
		return false
	}
}
