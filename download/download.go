package download

import (
	"embed"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"strings"

	"golang.org/x/mod/semver"

	autoio "gitlab.com/omnifi/automaatio/io"
	"gitlab.com/omnifi/automaatio/io/compression"
	autonet "gitlab.com/omnifi/automaatio/net"
	"gitlab.com/omnifi/automaatio/pulumi/environment"
)

//go:embed downloads.json
var AvailableDownloadsFile embed.FS

// AvailableDownloads provides a data structure over available
// Pulumi downloads. This is manually managed through the `downloads.json`
// file embedded into the package.
type AvailableDownloads struct {
	Version    string
	URL        string
	Variations []AvailableVariation
}

// AvailableVariations provides a structure for a variation of the Pulumi
// download for a given architecture (`Arch`) and operating system (`OS`).
type AvailableVariation struct {
	Arch     string
	Checksum string
	OS       string
	Tag      string
}

// GetAvailableDownloads loads the downloads into memory to be interrogated.
func GetAvailableDownloads() (*AvailableDownloads, error) {
	availableDownloads := &AvailableDownloads{}
	contents, err := AvailableDownloadsFile.ReadFile("downloads.json")

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(contents, &availableDownloads)

	if err != nil {
		return nil, err
	}

	return availableDownloads, nil
}

// Download fetches the Pulumi package and extracts it for us.
func Download() (string, error) {
	tempDir, err := os.MkdirTemp("", "pulumi")

	if err != nil {
		return "", err
	}

	filePath := path.Join(tempDir, "pulumi.tar.gz")
	downloadURL, checksum := getDownloadURLAndChecksum()
	filePath, err = autonet.DownloadAsFile(filePath, downloadURL)

	if err != nil {
		return "", err
	}

	verified := verifyDownload(filePath, checksum)

	if !verified {
		return "", &DownloadError{
			message: "pulumi download not verified",
		}
	}

	// Check if file exists.
	if fileInfo, err := os.Stat(filePath); err == nil {
		if fileInfo.Size() == 0 {
			return "", &DownloadError{
				message: fmt.Sprintf("pulumi download incomplete. size: %v", fileInfo.Size()),
			}
		}
	} else {
		return "", &DownloadError{
			message: fmt.Sprintf("error finding file: %s", err.Error()),
		}
	}

	reader, err := os.Open(filePath)

	if err != nil {
		return "", &DownloadError{
			message: fmt.Sprintf("error opening file: %s", err.Error()),
		}
	}

	pulumiPath := environment.GetWorkingPath()
	compression.ExtractTarGz(reader, pulumiPath)

	return environment.GetWorkingPath(), err
}

// isDownloaded verifies whether Pulumi has already been downloaded into the
// working path for use. This is used to prevent continuous downloads of Pulumi
// between executions.
func IsDownloaded() bool {
	pulumiPath := environment.GetExecutablePath()
	binPath := filepath.Join(pulumiPath, "pulumi")
	output, err := exec.Command(binPath, "version").Output()

	if err != nil {
		return false
	}

	semverComparison := semver.Compare("v3", string(output))

	if semverComparison == 1 {
		return true
	} else {
		return false
	}
}

// sourceDownload finds the download most suitable for this targert.
func sourceDownload(available *AvailableDownloads) (string, string) {
	os := runtime.GOOS
	arch := runtime.GOARCH

	for _, variation := range available.Variations {
		if strings.ToLower(variation.Arch) == arch && strings.ToLower(variation.OS) == os {
			url := strings.ReplaceAll(
				available.URL, "${variation}",
				strings.ReplaceAll(
					variation.Tag, "${version}",
					available.Version,
				),
			)

			checksum := variation.Checksum

			return url, checksum
		}
	}

	return "", ""
}

// getDownloadURLAndChecksum uses the available downloads to provide a URL
// and checksum SHA256 of the Pulumi download suitable for this target.
func getDownloadURLAndChecksum() (string, string) {
	availableDownloads, err := GetAvailableDownloads()

	if err != nil {
		return "", ""
	}

	return sourceDownload(availableDownloads)
}

// Verifies that the Pulumi package downloaded matches the checksum provided (and
// located in the `downloads.json“ embedded file).
func verifyDownload(downloadPath string, checksum string) bool {
	if checked, err := autoio.ChecksumSHA256(downloadPath, checksum); err == nil {
		return checked
	} else {
		return false
	}
}
