package download

import (
	"log"
	"os"
	"testing"

	"gitlab.com/omnifi/automaatio/pulumi/environment"
)

// TestLoadAvailableDownloads tests that the available downloads
// can be loaded correctly.
func TestLoadAvailableDownloads(t *testing.T) {
	availableDownloads, err := GetAvailableDownloads()

	if err != nil {
		t.Errorf("available downloads could not be loaded for pulumi: %s", err.Error())
	}

	log.Printf("version: %s", availableDownloads.Version)
}

// TestGetAvailable tests the ability to source the download URL.
func TestGetAvailable(t *testing.T) {
	availableDownloads, err := GetAvailableDownloads()

	if err != nil {
		t.Errorf("failed to get available downloads for pulumi: %s", err.Error())
	}

	availableURL, _ := sourceDownload(availableDownloads)

	if len(availableURL) == 0 {
		t.Errorf("no available download found")
	}
}

// TestDownload tests the download functionality, including
// checksum verification.
func TestDownload(t *testing.T) {
	path, err := Download()

	if err != nil {
		t.Errorf("pulumi failed to download: %s", err.Error())
	}

	if fileInfo, err := os.Stat(path); err == nil {
		if fileInfo.Size() == 0 {
			t.Errorf("file is 0 in size")
		}
	} else {
		t.Errorf("file could not be checked")
	}
}

// TestIsNotDownloaded tests the check regarding whether the
// download is where it is expected on the system.
func TestIsNotDownloaded(t *testing.T) {
	pulumiPath := environment.GetExecutablePath()
	err := os.RemoveAll(pulumiPath)

	if err != nil {
		t.Errorf("pulumi was discoverable: %s", err.Error())
	}

	downloaded := IsDownloaded()

	if downloaded {
		t.Errorf("pulumi already exists.")
	}
}

// TestIsDownloaded tests the validation when the download has already
// been downloaded.
//
// ## Dependencies
//
// This is dependendant on the `download` method operating as expected. Any
// issues with this test may be a result of that method having an issue.
func TestIsDownloaded(t *testing.T) {
	_, err := Download()

	if err != nil {
		t.Errorf("pulumi was not downloaded: %s", err.Error())
	}

	downloaded := IsDownloaded()

	if !downloaded {
		t.Errorf("pulumi was not downloaded correctly")
	}
}
