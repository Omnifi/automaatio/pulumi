package download

import (
	"fmt"
)

// DownloadError is used to signal a failed operation due to
// an issue with the Pulumi downloads.
type DownloadError struct {
	// Error to pass.
	message string
}

// Returns the error message.
func (pde *DownloadError) Error() string {
	return fmt.Sprintf("pulumi download error: %v", pde.message)
}
